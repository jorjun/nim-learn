## @jorjun (Vvi ☉ iv ☽ xiii)

import strformat

proc distance*(left: string, right: string): int =
    if left.len != right.len:
        raise newException(ValueError, "Mismatched DNA length, left: {left.len}, right: {right.len}".fmt)

    var dist = 0
    for ix, l_chr in left:
        dist += cast[int](l_chr != right[ix])

    return dist
