## @jorjun (Vvi ☉ iv ☽ xiv)

proc toRna*(strand: string): string =
    const
        DNA = "GCTA"
        RNA = "CGAU"

    for nuc in strand:
        let rx = DNA.find(nuc)
        if rx<0:
            raise newException(ValueError, "Bad nucleotide")
        result.add(RNA[rx])
