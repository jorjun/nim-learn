import strformat

proc twoFer*(who="you"): string =
    return fmt"One for {who}, one for me."
