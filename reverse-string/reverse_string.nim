# @jorjun (Vvi ☉ iv ☽ xiv)

proc reverse*(input: string): string =
    for ch in input:
        result = ch & result
