# @jorjun (Vvi ♈iv▲ )

import
    tables

proc age*(planet: string, age_sec: SomeInteger): SomeFloat =
    const planetYear = {
        "Mercury" :0.2408467,
        "Venus" : 0.61519726,
        "Earth" : 1.0,
        "Mars" : 1.8808158,
        "Jupiter" : 11.862615,
        "Saturn" : 29.447498,
        "Uranus" : 84.016846,
        "Neptune" : 164.79132,
    }.toTable

    let age_year = age_sec.float / 31557600
    result = age_year / planetYear[planet]
