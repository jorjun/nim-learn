# @jorjun (Vvi ☉ iv ☽ xii)

import
    sequtils,
    unicode,
    tables

proc isIsogram*(word:string): bool =
    let freq =
        toCountTable(
            word.toRunes.filterIt(it.isAlpha)
            .mapIt(it.toLower)
        )
    return not toSeq(freq.values).anyIt(it > 1)
