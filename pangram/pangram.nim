import sequtils, strutils

proc isPangram*(input: string): bool =
    return {'a'..'z'}.allIt(it in input.toLower())
