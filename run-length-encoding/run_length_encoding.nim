# @jorjun (Vvi ☉ iv ☽ xiv)

import
    strutils,
    parseutils

proc encode*(text: string): string =
    iterator itr_group(text: string): string =
        var ix = 0
        while ix < text.len:
            let n_repeats = skipWhile(text, {text[ix]}, ix)
            yield text[ix].repeat n_repeats # eg. aaaa
            inc ix, n_repeats

    for inp in itr_group(text):
        if inp.len > 1:
            result.add $inp.len
        result.add inp[0]

proc decode*(text: string): string =
    var
        ix = 0
        n_repeats = 0

    while ix < text.len:
        if text[ix].isDigit:
            inc ix, parseInt(text, n_repeats, ix)
            result &= text[ix].repeat n_repeats # eg. aaaa
        else:
            result &= text[ix]
        inc ix
