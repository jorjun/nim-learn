# @jorjun (Vvi ☉ iv ☽ xiv)

import
    strutils

proc roman*(input: int): string =
    type
        Numerals {.pure.} = enum
            M, CM, D, CD, C, XC, L, XL, X, IX, V, IV, I

    const roman: array[Numerals, int] =
        [1000, 900,  500, 400, 100,  90, 50,  40, 10,  9,   5,  4,   1]

    var n_repeat,
        number = input

    for numeral in Numerals.items:
        n_repeat = number div roman[numeral]
        if n_repeat > 0:
            number = number mod roman[numeral]
            result &= ($numeral).repeat n_repeat

echo roman(2022)